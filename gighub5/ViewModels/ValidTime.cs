﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web;

namespace gighub5.ViewModels
{
    public class ValidTime : ValidationAttribute
    {
        public override bool IsValid(object value)
    {
        DateTime dateTime;

        var isValid = DateTime.TryParseExact(Convert.ToString(value), "HH:MM", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime);
        return (isValid );
    }
}
}
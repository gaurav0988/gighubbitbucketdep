﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gighub5.Models
{
    public class Gig
    {
        public int id { get; set; }
        
        public ApplicationUser Artist { get; set; }
        [Required]
        public String ArtistId { get; set; }
        public DateTime DateTime { get; set; }
        [Required]
        [StringLength(255)]
        public String Venue { get; set; }
        
        public Genre Genre { get; set; }
        [Required]
        public Byte GenreId { get; set; }


    }
}